package com.example.kareem.paid

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.example.kareem.paid.databinding.ActivityMainBinding
import com.example.kareem.paid.databinding.AddItemsDialogBinding
import com.example.kareem.paid.ui.main.dashboard.DashboardFragment
import com.example.kareem.paid.ui.main.home.HomeFragment
import com.example.kareem.paid.ui.main.notifications.NotificationFragment
import com.example.kareem.paid.utils.ext.replaceFragment
import com.example.kareem.paid.utils.ext.setFragment
import kotlinx.android.synthetic.main.fragment_home.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    private val binding: ActivityMainBinding by lazy {
        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setFragment(R.id.frame_main_content, ::HomeFragment)
        binding.bottomNavMain.setOnNavigationItemSelectedListener(this)

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            binding.bottomNavMain.selectedItemId -> return false
            R.id.navigation_home -> replaceFragment(R.id.frame_main_content, ::HomeFragment)
            R.id.navigation_dashboard -> replaceFragment(R.id.frame_main_content, ::DashboardFragment)
            R.id.navigation_notifications -> replaceFragment(R.id.frame_main_content, ::NotificationFragment)
            else -> throw IllegalStateException("Not founded this ID")
        }
        return true
    }


}
