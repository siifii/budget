package com.example.kareem.paid.ui.main.home


import android.app.AlertDialog
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.kareem.paid.R
import com.example.kareem.paid.R.id.fab_add_item
import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val vg = inflater.inflate(R.layout.fragment_home, container, false)

        fab_add_item.apply {
            setOnClickListener {
                val builder = AlertDialog.Builder(activity)
                val binding = DataBindingUtil.inflate<ViewDataBinding>(
                        LayoutInflater.from(activity), R.layout.add_items_dialog, null, false)
                builder.setView(binding.root)
                        .setPositiveButton(R.string.add, { _, i ->
                        }
                        )
                        .setNegativeButton(R.string.cancel, { _, i -> }).show()
            }
        }
        return vg
    }

}


